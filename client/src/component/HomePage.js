import React, {Component} from 'react';
import axios from 'axios';
import {Link} from "react-router-dom";
import {PATH} from "../utils/util";


class HomePage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    componentDidMount() {
        this.queryLessons();
        this.queryExtraLessons();
    };

    queryLessons = () => {
        axios.get(PATH + 'lessons').then(res => {
            console.log(res.data);
        })
    };

    queryExtraLessons = (id) => {
        axios.get(PATH + 'lessons/extra?lessonId=' + id).then(res => {
            console.log(res.data);
        })
    };


    render() {
        // const data = this.state.data;
        return (
            <>
                <div className="container-fluid">
                    <section className="header">
                        <ul className="d-flex ml-0">
                            <Link to="/home">
                                <button className="btn allBtn"><b>"Sayt nomi yoki logosi"</b></button>
                            </Link>
                            <Link to="/prolang">
                                <button className="btn allBtn"><b>Dasturlash tillari</b></button>
                            </Link>
                            <Link to="/useful">
                                <button className="btn allBtn"><b>Foydali saytlar</b></button>
                            </Link>
                            <Link to="/team">
                                <button className="btn allBtn"><b>Bizning Jamoa</b></button>
                            </Link>
                            <Link to="/home">
                                <button className="btn allBtn"><b>Biz haqimizda</b></button>
                            </Link>
                            <Link to="/login">
                                <button className="btn allBtn loginBtn float-right"><b>Kirish</b></button>
                            </Link>
                        </ul>
                        <hr/>
                    </section>
                </div>
                <div className="container">
                    <section className="body">
<h1> body qismi</h1>
                    </section>
                </div>
                <div className="container-fluid">
                    <section className="footer">
                        asdddddddddd
                    </section>
                </div>
            </>
        );
    }
}

export default HomePage;