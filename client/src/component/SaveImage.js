import React, {Component} from 'react';
import axios from "axios";

class SaveImage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            imagesId: ''
        }
    }


    saveImage = (e) => {
        document.getElementById("selectedFileName").innerHTML = document.getElementById("input-file").value;
        let formData = new FormData();
        formData.append("request", e.target.files[0]);
        axios({
            url: 'http://localhost/api/file/saveFile',
            method: 'post',
            data: formData,
            headers: {
                "Content-Type": "multipart/form-data"
            }
        }).then((res) => {
            this.setState({
                imagesId: res.data.object.id
            })
        })

    };

    render() {
        return (
            <div>
                <label className="upload-btn">
                    <input className="input-files" type="file" id="input-file"
                           onChange={this.saveImage}/>
                    <span id="selectedFileName"><b>Choose File</b></span>
                </label>
            </div>
        );
    }
}

export default SaveImage;