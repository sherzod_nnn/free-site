package com.com.security;

import com.com.entity.User;
import com.com.entity.enums.RoleName;
import com.com.exceptions.ResourceNotFound;
import com.com.payload.ApiResponse;
import com.com.payload.ReqUser;
import com.com.repository.RoleRepository;
import com.com.repository.UserRepository;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Collections;
import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;


    public AuthService(@Lazy UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    @Override

    public UserDetails loadUserByUsername(String phoneNumber) throws UsernameNotFoundException {
        return (UserDetails) userRepository.findByPhoneNumber(phoneNumber).orElseThrow(() -> new UsernameNotFoundException(phoneNumber));
    }

    public UserDetails loadUserById(UUID userId) {
        return (UserDetails) userRepository.findById(userId).orElseThrow(() -> new UsernameNotFoundException("User id not found: " + userId));
    }

    public ApiResponse saveUser(ReqUser reqUser) {

        if (reqUser.getId() != null) {
            User user = userRepository.findById(reqUser.getId()).orElseThrow(() -> new UsernameNotFoundException("User id not found: " + reqUser.getId()));
            userRepository.save(user);
            return new ApiResponse("Successfully edited old user", true);
        } else {

            try {
                User user = new User();
                user.setFirstName(reqUser.getFirstName());
                user.setLastName(reqUser.getLastName());
                user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
                user.setPhoneNumber(reqUser.getPhoneNumber());
                user.setRoles(roleRepository.findAllByNameIn(
                        Collections.singletonList(RoleName.ROLE_USER)));
                userRepository.save(user);
                return new ApiResponse("success", true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new ApiResponse("error", false, reqUser);
    }

    public ApiResponse editUser(ReqUser reqUser) {
        User user = userRepository.findById(reqUser.getId()).orElseThrow(() -> new ResourceNotFound("User", "id", reqUser.getId()));
        user.setFirstName(reqUser.getFirstName());
        user.setLastName(reqUser.getLastName());
        user.setPhoneNumber(reqUser.getPhoneNumber());
        user.setPassword(reqUser.getPassword());
        userRepository.save(user);
        return new ApiResponse("success", true);
    }


    public ApiResponse deleteUser(UUID userId) {
        try {
            userRepository.deleteById(userId);
            return new ApiResponse("success", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error", false);
    }

    public ApiResponse changeEnabled(UUID userId) {
        return new ApiResponse("User " + (userRepository.changeEnabledById(userId) ? "enabled" : "disabled"), true);
    }
}

