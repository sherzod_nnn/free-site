package com.com.controller;


import com.com.entity.User;
import com.com.payload.ApiResponse;
import com.com.payload.JwtResponse;
import com.com.payload.ReqSignIn;
import com.com.payload.ReqUser;
import com.com.repository.UserRepository;
import com.com.security.AuthService;
import com.com.security.CurrentUser;
import com.com.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


@RestController
@RequestMapping("/api/user")
public class AuthController {
    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    AuthenticationManager authenticate;

    @Autowired
    AuthService authService;

    @Autowired
    UserRepository userRepository;



    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody ReqSignIn reqSignIn) {
        return ResponseEntity.ok(getApiToken(reqSignIn.getPhoneNumber(), reqSignIn.getPassword()));
    }

    public HttpEntity<?> getApiToken(String phoneNumber, String password) {
        Authentication authentication = authenticate.authenticate(
                new UsernamePasswordAuthenticationToken(phoneNumber, password)
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtTokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtResponse(jwt));
    }


    @GetMapping
    public HttpEntity<?> getAll() {
        List<User> all = userRepository.findAll();
        return ResponseEntity.ok(all);
    }

    @GetMapping("/getUserByPhone")
    public HttpEntity<?> getUser(@RequestParam String phone) {
        phone = phone.trim();
        phone = phone.startsWith("+") ? phone : "+" + phone;
        Optional<User> byPhoneNumber = userRepository.findByPhoneNumber(phone);
        return ResponseEntity.status(200).body(byPhoneNumber.isPresent() ? byPhoneNumber.get() : "null");
    }




    @GetMapping("/me")
    public HttpEntity<?> userMe(@CurrentUser User user){
        return ResponseEntity.status(user!=null?200:409).body(user);
    }




    @PostMapping("/save")
    public HttpEntity<?> saveUser(@RequestBody ReqUser reqUser) {
        ApiResponse apiResponse = authService.saveUser(reqUser);
        if (apiResponse.isSuccess()){
            return getApiToken(reqUser.getPhoneNumber(),reqUser.getPassword());
        }
        return ResponseEntity.status(409).body(apiResponse);
    }

    @PutMapping("/edit")
    public HttpEntity<?> editUser(@RequestBody ReqUser reqUser) {
        ApiResponse apiResponse = authService.editUser(reqUser);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @DeleteMapping("/delete")
    public HttpEntity<?> deleteUser(@RequestParam UUID userId) {
        ApiResponse apiResponse = authService.deleteUser(userId);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PutMapping("/changeEnabled")
    public HttpEntity<?> changeEnabled(@RequestParam UUID userId) {
        ApiResponse apiResponse = authService.changeEnabled(userId);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200:409).body(apiResponse);
    }
}
