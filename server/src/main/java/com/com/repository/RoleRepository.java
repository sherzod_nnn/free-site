package com.com.repository;

import com.com.entity.Role;
import com.com.entity.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
    List<Role> findAllByNameIn(List<RoleName> name);
}
