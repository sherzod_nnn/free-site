package com.com.entity;

import com.com.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.FetchType.LAZY;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class Extralesson extends AbsEntity {

    @Column(nullable = false)
    private String name;

    private String code;

    private String text;

    @ManyToOne(fetch=LAZY)
    private Lesson lesson;

    @ManyToMany(fetch = LAZY)
    @JoinTable(name = "image_extra", joinColumns = {@JoinColumn(name = "extralesson_id")},
            inverseJoinColumns = {@JoinColumn(name = "attachment_id")})
    private List<Attachment> images;
}
