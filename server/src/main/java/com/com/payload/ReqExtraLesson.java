package com.com.payload;

import com.com.entity.Attachment;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class ReqExtraLesson {

    private UUID id;

    private String name;

    private String text;

    private String code;

    private UUID lessonId;

    private List<Attachment> attachmentIds;
}
